module gitlab.com/dn13/cert-manager-webhook-oci

go 1.13

require (
	github.com/jetstack/cert-manager v0.13.1
	github.com/oracle/oci-go-sdk v15.7.0+incompatible
	k8s.io/apiextensions-apiserver v0.17.0
	k8s.io/apimachinery v0.17.0
	k8s.io/client-go v0.17.0
	k8s.io/klog v1.0.0
)
